package davide.pugliese;

import davide.pugliese.v2.DirectorIsoButane;
import davide.pugliese.v2.DirectorNButane;

public class Main {

  public static void main(String... args) {

    //        Director.calculate();

    DirectorNButane.calculate();
    DirectorIsoButane.calculate();
  }
}
