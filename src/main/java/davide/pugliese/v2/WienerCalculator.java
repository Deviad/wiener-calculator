package davide.pugliese.v2;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
public class WienerCalculator {

  public static void calculate(final Graph graph, final Molecule molecule) {

    final Map<Integer, Map<String, Integer>> distanceMap = new LinkedHashMap<>();
    var distanceMapKey = 0;

    final GraphService graphService = new GraphService();

    for (int nodeIntRepresentation = 1;
        nodeIntRepresentation <= graph.getNodes().size();
        nodeIntRepresentation++) {

      final Graph cleanGraph =
          calculateShortestPathFromSource(molecule, graphService, nodeIntRepresentation);

      distanceMapKey =
          addToDistanceMap(distanceMap, distanceMapKey, nodeIntRepresentation, cleanGraph);
    }
    System.out.println(distanceMap + "\n");
    final var wienerNumber =
        distanceMap.values().stream()
            .map(Map::values)
            .flatMap(Collection::stream)
            .mapToInt(Integer::intValue)
            .sum();
    System.out.println(molecule.name() + " Wiener number: " + wienerNumber + "\n");
  }

  private static Graph calculateShortestPathFromSource(
      Molecule molecule, GraphService graphService, int nodeIntegerRepresentation) {
    final Graph cleanGraph = graphService.buildGraph(molecule);
    final var label = Integer.toString(nodeIntegerRepresentation);
    final Node filteredNode =
        cleanGraph.getNodes().stream()
            .filter(node -> node.getName().equals(label))
            .findFirst()
            .orElseThrow();

    Dijkstra.calculateShortestPathFromSource(filteredNode);
    return cleanGraph;
  }

  private static int addToDistanceMap(
      Map<Integer, Map<String, Integer>> distanceMap,
      int distanceMapKey,
      int nodeIntRepresentation,
      Graph cleanGraph) {
    for (final Node node : cleanGraph.getNodes()) {
      if (Integer.parseInt(node.getName()) > nodeIntRepresentation) {
        System.out.println(String.format("%d, %d", nodeIntRepresentation, Integer.parseInt(node.getName())));
      }

      if (Integer.parseInt(node.getName()) > nodeIntRepresentation) {
        distanceMap.put(
            distanceMapKey,
            new LinkedHashMap<>(Collections.singletonMap(node.getName(), node.getDistance())));
        distanceMapKey++;
      }
    }
    return distanceMapKey;
  }
}
