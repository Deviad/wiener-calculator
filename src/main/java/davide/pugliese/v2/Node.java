package davide.pugliese.v2;

import lombok.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Node {

  private String name;

  private Integer distance = Integer.MAX_VALUE;

  private Map<Node, Integer> adjacentNodes = new HashMap<>();

  public Node(String name) {
    this.name = name;
  }
}
