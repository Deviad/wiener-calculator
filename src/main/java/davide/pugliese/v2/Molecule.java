package davide.pugliese.v2;

import java.util.function.Function;

public enum Molecule {
    ISOBUTANE(GraphService::buildIsoButaneGraph),
    NBUTANE(GraphService::buildNButaneGraph);
    public final Function<GraphService, Graph> graphFactory;
    Molecule(Function<GraphService, Graph> graphFactory) {
        this.graphFactory = graphFactory;
    }

}
