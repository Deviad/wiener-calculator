package davide.pugliese.v2;

import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Stream;
@SuppressWarnings("Duplicates")
@NoArgsConstructor
public class GraphService {

    public Graph buildIsoButaneGraph() {

        final Node node1 = new Node("1");
        final Node node2 = new Node("2");
        final Node node3 = new Node("3");
        final Node node4 = new Node("4");

        node1.setAdjacentNodes(Collections.singletonMap(node2, 1));

        node2.setAdjacentNodes(
                Stream.of(Collections.singletonMap(node1, 1), Collections.singletonMap(node3, 1), Collections.singletonMap(node4, 1))
                        .flatMap(map -> map.entrySet().stream())
                        .collect(LinkedHashMap::new, (m, e)-> m.put(e.getKey(), e.getValue()), Map::putAll)
        );
        node3.setAdjacentNodes(Collections.singletonMap(node2, 1));
        node4.setAdjacentNodes(Collections.singletonMap(node2, 1));

        return new Graph(new LinkedHashSet<>(Arrays.asList(node1, node2, node3, node4)));
    }

    public Graph buildNButaneGraph() {
        final Node node1 = new Node("1");
        final Node node2 = new Node("2");
        final Node node3 = new Node("3");
        final Node node4 = new Node("4");

        node1.setAdjacentNodes(Collections.singletonMap(node2, 1));
        node2.setAdjacentNodes(
                Stream.of(Collections.singletonMap(node1, 1), Collections.singletonMap(node3, 1))
                        .flatMap(map -> map.entrySet().stream())
                        .collect(LinkedHashMap::new, (m, e)-> m.put(e.getKey(), e.getValue()), Map::putAll)
        );
        node3.setAdjacentNodes(
                Stream.of(Collections.singletonMap(node2, 1), Collections.singletonMap(node4, 1))
                        .flatMap(map -> map.entrySet().stream())
                        .collect(LinkedHashMap::new, (m, e)-> m.put(e.getKey(), e.getValue()), Map::putAll)
        );
        node4.setAdjacentNodes(Collections.singletonMap(node3, 1));

       return new Graph(new LinkedHashSet<>(Arrays.asList(node1, node2, node3, node4)));
    }


    public Graph buildGraph(Molecule type) {
        return type.graphFactory.apply(this);
    }

}
