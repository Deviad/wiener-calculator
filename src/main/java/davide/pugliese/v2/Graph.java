package davide.pugliese.v2;

import lombok.*;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Graph implements Iterable<Node> {

    private Set<Node> nodes = new LinkedHashSet<>();

    @Override
    public Iterator<Node> iterator() {
        return nodes.iterator();
    }
}
