package davide.pugliese.v2;

import lombok.Data;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

@Data
public class Dijkstra {

  public static void calculateShortestPathFromSource(final Node source) {

    source.setDistance(0);

    Set<Node> settledNodes = new HashSet<>();
    Set<Node> unsettledNodes = new HashSet<>();
    unsettledNodes.add(source);

    while (unsettledNodes.size() != 0) {


      Node lowestDistanceNode = getLowestDistanceNode(unsettledNodes);
      unsettledNodes.remove(lowestDistanceNode);
      for (Entry<Node, Integer> adjacencyPair : lowestDistanceNode.getAdjacentNodes().entrySet()) {
        Node adjacentNode = adjacencyPair.getKey();
        Integer edgeWeight = adjacencyPair.getValue();

        if (!settledNodes.contains(adjacentNode)) {
          calculateMinimumDistance(adjacentNode, edgeWeight, lowestDistanceNode);
          unsettledNodes.add(adjacentNode);
        }
      }
      settledNodes.add(lowestDistanceNode);
    }
  }

  // calculateMinimumDistance() method compares the actual distance with the newly calculated one
  // while following the newly explored path
  private static void calculateMinimumDistance(
    Node adjacentNode, Integer edgeWeight, Node lowestDistanceNode) {
    Integer lowestDistanceNodeDistance = lowestDistanceNode.getDistance();

    if (lowestDistanceNodeDistance + edgeWeight < adjacentNode.getDistance()) {
      adjacentNode.setDistance(lowestDistanceNodeDistance + edgeWeight);
    }
  }

  // Extract the node with the lowest distance from the unsettled nodes set
  private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
    Node lowestDistanceNode = null;
    int lowestDistance = Integer.MAX_VALUE;
    for (Node node : unsettledNodes) {
      int nodeDistance = node.getDistance();
      if (nodeDistance < lowestDistance) {
        lowestDistance = nodeDistance;
        lowestDistanceNode = node;
      }
    }
    return lowestDistanceNode;
  }
}
