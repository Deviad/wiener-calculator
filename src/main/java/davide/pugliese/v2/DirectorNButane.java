package davide.pugliese.v2;

@SuppressWarnings("Duplicates")
public class DirectorNButane {

  public static void calculate() {
   final GraphService graphService = new GraphService();
   final Graph graph = graphService.buildGraph(Molecule.NBUTANE);
    WienerCalculator.calculate(graph, Molecule.NBUTANE);
  }
}
