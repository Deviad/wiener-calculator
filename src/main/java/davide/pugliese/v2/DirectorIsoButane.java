package davide.pugliese.v2;

@SuppressWarnings("Duplicates")
public class DirectorIsoButane {

  public static void calculate() {
    final GraphService graphService = new GraphService();
    final Graph graph = graphService.buildGraph(Molecule.ISOBUTANE);
    WienerCalculator.calculate(graph, Molecule.ISOBUTANE);
  }
}
